"use strict"

/*

він означає асинхронний JavaScript за його допомогою можна отримувати дані,
 з сервера, без перезавантаження сторінки.

*/

//__________________________________________________________________________

const container = document.querySelector(".container")

fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(res => res.json())
    .then(data => {
        console.log(data)
        let id = 1;
        data.forEach(({characters, episodeId, name, id, openingCrawl}) => {

            container.insertAdjacentHTML("beforeend", `
                                <div class="movie">
                                <p class="episode">episode:${episodeId}</p>
                                <h4>${name}</h4>
                                    <ul class="class-${id}">
                               
                                    </ul>
                                <p>${openingCrawl}</p>
                                </div>    
                              `);

            let clasId = document.querySelector(`.class-${id}`);


            characters.forEach(el => {
                fetch(el)
                    .then(res => res.json())
                    .then(data => {
                        clasId.insertAdjacentHTML("beforeend", `<li>${data.name}</li>`);
                    });

            });
        });
        id++;
    })
    .catch(err => console.log(err))

